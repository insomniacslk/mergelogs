from distutils.core import setup

setup(
    name='mergelogs',
    version='0.1',
    author='Andrea Barberio',
    author_email='insomniac@slackware.it',
    description='Tool to merge and analyze OpenStack logs',
    url='https://bitbucket.org/insomniacslk/mergelogs',
    packages=['mergelogs'],
)
