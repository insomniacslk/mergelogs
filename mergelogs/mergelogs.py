#!/usr/bin/env python

from __future__ import print_function

import os
import re
import sys
import datetime

try:
    import dateutil.parser
    import dateutil.tz
except ImportError:
    print('Failed importing dateutil. Please install it first. '
          'See https://github.com/dateutil/dateutil/')
    sys.exit(1)

try:
    import grec
    can_highlight_grep = True
except ImportError:
    can_highlight_grep = False


def to_datetime(timestamp, tzinfo=None):
    '''
    Try to convert a timestamp to a datetime.datetime object. The input can be
    either a string, a datetime.datetime or None.
    '''
    if timestamp is None:
        return None
    if isinstance(timestamp, datetime.datetime):
        return timestamp
    try:
        timestamp = dateutil.parser.parse(timestamp)
        return add_timezone(timestamp, tzinfo)
    except ValueError:
        return None


def timestamp_is_in_range(ts, start=None, end=None):
    '''
    Return whether a timestamp is in the given range.
    '''
    ts = to_datetime(ts)
    start = to_datetime(start)
    end = to_datetime(end)
    if start and ts < start:
            return False
    if end and ts > end:
        return False
    return True


class LogEntry(object):
    def __init__(self, timestamp=None, logfile=None, message='', prefix=''):
        self.timestamp = timestamp
        self.logfile = logfile
        self.message = message
        self.prefix = prefix
        self._num_lines = None
        self._grep_pattern = None
        self._grep_used_regex = False

    @property
    def num_lines(self):
        if self._num_lines is None:
            self._num_lines = len(self.message.splitlines())
        return self._num_lines

    def __str__(self):
        if self._grep_pattern is None:
            return self.message
        else:
            matcher = grec.Matcher()
            if self._grep_used_regex:
                matcher.add_group_pattern(self._grep_pattern, ['red'])
            else:
                matcher.add_pattern(self._grep_pattern, 'red')
            self._grep_pattern = None
            return str(matcher.match(self.message))

    def __repr__(self):
        return '<{cls} (timestamp={t!r}, logfile={l!r}, message={m!r})'.format(
            cls=self.__class__.__name__,
            t=self.timestamp,
            l=self.logfile,
            p=self.prefix,
            m=self.message,
        )

    def grep(self, pattern, case_sensitive=False, regex=False, invert=False,
             verbose=False):
        '''
        Search for a pattern in the log entry. If regex is True, the pattern is
        treated as a regular expression.
        '''
        if regex:
            match = self.match(pattern, case_sensitive=case_sensitive,
                               verbose=verbose)
            matching = True if match else False
        else:
            if case_sensitive:
                message = self.message
            else:
                pattern = pattern.lower()
                message = self.message.lower()
            matching = True if pattern in message else False
        # act like grep -v
        if invert:
            matching = not matching
        if matching and can_highlight_grep:
            # colorize the matches
            if regex:
                self._grep_used_regex = True
                self._grep_pattern = match.re if match is not None else None
            else:
                self._grep_used_regex = False
                self._grep_pattern = pattern

        if matching:
            return self
        else:
            return None

    def match(self, pattern, case_sensitive=False, message=None,
              verbose=False):
        '''
        Applies a regex to the log message and returns the match object if
        matching, None otherwise. Just like re.match .
        '''
        if message is None:
            message = self.message

        flags = 0
        if not case_sensitive:
            flags |= re.IGNORECASE
        if verbose:
            flags |= re.VERBOSE
        rx = re.compile(pattern, flags)
        return rx.match(message)


class LogEntries(object):
    def __init__(self, entries):
        self.entries = entries
        self.total_entries = len(entries)
        self.total_lines = sum([entry.num_lines for entry in entries])
        self._log_files = None
        try:
            self.oldest = self.entries[0].timestamp
        except IndexError:
            self.oldest = None
        try:
            self.newest = self.entries[-1].timestamp
        except IndexError:
            self.newest = None

    @property
    def log_files(self):
        if self._log_files is None:
            self._log_files = set([e.logfile for e in self.entries])
        return self._log_files

    def __iter__(self):
        return iter(self.entries)

    def __getitem__(self, index):
        if isinstance(index, slice):
            return LogEntries(self.entries[index.start:index.stop:index.step])
        return self.entries[index]

    def __len__(self):
        return len(self.entries)

    def __repr__(self):
        return('<{cls} (total_entries={te!r}, total_lines={tl!r}, '
               'num_log_files={nlf!r})>'.format(
                   cls=self.__class__.__name__,
                   te=self.total_entries,
                   tl=self.total_lines,
                   nlf=len(self.log_files) if self.log_files else None,
                   ))

    def __str__(self):
        ret = ''
        for entry in self.entries:
            ret += str(entry)
        return ret

    def in_range(self, start, end):
        entries_in_range = []
        for entry in self.entries:
            if timestamp_is_in_range(entry.timestamp, start, end):
                entries_in_range.append(entry)
        return LogEntries(entries_in_range)

    def holes(self, hole_size=datetime.timedelta(seconds=5 * 60 * 60)):
        '''
        Find holes in the log files. Default hole size is 1 hour
        '''
        previous_entry = None
        holes = []
        for entry in self.entries:
            if previous_entry is None:
                previous_entry = entry
                continue
            if entry.timestamp - previous_entry.timestamp > hole_size:
                holes.append((previous_entry, entry))
            previous_entry = entry
        return holes

    def stats(self, verbose=False, as_dict=False):
        if as_dict:
            statsdict = {
                'total_lines': self.total_lines,
                'total_entries': self.total_entries,
                'oldest': self.oldest,
                'newest': self.newest,
            }
            if verbose:
                statsdict['holes'] = self.holes()
                statsdict['logfiles'] = self.log_files
            return statsdict
        else:
            return self.printable_stats(verbose)

    def printable_stats(self, verbose=False):
        printable = '''Total log lines: {tl}
Total log entries: {te}
Oldest entry timestamp: {fet}
Newest entry timestamp: {let}
Total log files: {tlf}'''.format(
                  tl=self.total_lines,
                  te=self.total_entries,
                  fet=self.oldest,
                  let=self.newest,
                  tlf=len(self.log_files),
              )
        if verbose:
            # add details on the log files
            padding = ' ' * 8
            for log_file in sorted(self.log_files):
                printable += '\n{padding}{name}'.format(
                    padding=padding, name=log_file)
        printable += '''\nDiscarded log files: {dlf}'''.format(
            dlf=len(self.log_files) - len(self.log_files)
            if self.log_files is not None else 'N/a'
        )
        if verbose:
            # add details on the discarded log files
            padding = ' ' * 8
            discarded_files = set(self.log_files).difference(
                set(self.log_files))
            for log_file in sorted(discarded_files):
                printable += '\n{padding}{name}'.format(
                    padding=padding, name=log_file)
            # print holes in logs
            holes = self.holes()
            printable += '\nHoles (min 1h) found: {nh}\n'.format(nh=len(holes))
            for hole in holes:
                start = hole[0].timestamp
                end = hole[1].timestamp
                printable += '{padding}{start} ~ {end} (length: {l})\n'.format(
                    padding=padding,
                    start=start,
                    end=end,
                    l=end-start,
                )
        return printable

    def grep(self, pattern, case_sensitive=False, regex=False, invert=False,
             verbose=False):
        '''
        Search for a pattern in the log entry. If regex is True, the pattern is
        treated as a regular expression.
        '''
        ret = []
        for line in self.entries:
            match = line.grep(pattern, case_sensitive, regex, invert, verbose)
            if match:
                ret.append(match)
        return LogEntries(ret)

    def match(self, pattern, case_sensitive=False):
        '''
        Applies a regex to the log entries and returns a list of either match
        objects and None, whether the pattern matches or not.
        '''
        ret = []
        for line in self.entries:
            match = line.match(pattern, case_sensitive)
            if match:
                ret.append(match)
        return ret

    def head(self, num_entries=10):
        '''
        Return at most the first `num_entries' of the logs, like head(1)
        '''
        if num_entries < 1:
            raise ValueError('The number of lines must be greater than 0')
        return LogEntries(self[:num_entries])

    def tail(self, num_entries=10):
        '''
        Return at most the last `num_entries' of the logs, like tail(1).
        '''
        if num_entries < 1:
            raise ValueError('The number of lines must be greater than 0')
        return LogEntries(self[-num_entries:])

    def in_files(self, pattern, case_sensitive=False):
        '''
        Filter the log entries by file name
        '''
        if not case_sensitive:
            flags = re.IGNORECASE
        else:
            flags = 0
        rx_in_files = re.compile(pattern, flags)

        entries = []
        for entry in self.entries:
            if rx_in_files.match(entry.logfile):
                entries.append(entry)
        return LogEntries(entries)

    def time_range(self):
        '''
        return a 2-tuple containing the oldest and the newest timestamps
        '''
        return self.oldest, self.newest


def merge(filelist, pattern=None, is_regex=False, case_sensitive=False,
          default_tzinfo=None):
    # if any of the files is a directory, merge all the files in the directory
    # too
    new_filelist = []
    for filename in filelist:
        if os.path.isdir(filename):
            dirname = filename
            new_filelist.extend(
                [os.path.join(dirname, fname) for fname in os.listdir(filename)]
            )
        else:
            new_filelist.append(filename)
    # build the file list and merge them
    prefix_and_file_list = []
    for filename in new_filelist:
        prefix_and_file_list.append((filename, filename))
    return merge_with_prefix(prefix_and_file_list, pattern, is_regex,
                             case_sensitive, default_tzinfo)


def add_timezone(timestamp, tzinfo=None):
    '''
    Add the given tzinfo to a timestamp if it is a naive timestamp
    '''
    if timestamp is None:
        return None
    if tzinfo is None:
        tzinfo = dateutil.tz.tzutc()
    return timestamp.replace(tzinfo=dateutil.tz.tzutc())


def extract_timestamp(line, default_tzinfo=None):
    '''
    Horrible way to detect the file type and get the timestamp accordingly
    '''
    timestamp = None
    try:
        ts_string = ' '.join(line.split()[:2])
        timestamp = dateutil.parser.parse(ts_string)
    except ValueError:
        try:
            ts_string = ' '.join(line.split()[:5]).strip('[]')
            timestamp = dateutil.parser.parse(ts_string)
        except ValueError:
            try:
                ts_string = (' '.join(line.split()[3:5])
                             .strip('[]')
                             .replace(':', ' ', 1))
                timestamp = dateutil.parser.parse(ts_string)
            except (IndexError, ValueError):
                pass
    return add_timezone(timestamp, default_tzinfo)


def merge_with_prefix(prefix_and_file_list, pattern=None, is_regex=False,
                      case_sensitive=False, default_tzinfo=None):
    '''
    Merge a set of log files and sort them by timestamp. Optionally return only
    the lines in the given time range.
    '''
    entries = []
    for prefix, filename in prefix_and_file_list:
        with open(filename, 'rb') as fd:
            entry = LogEntry(logfile=filename, prefix=prefix)
            for line in fd:
                if entry:
                    entry.message += line
                entry.timestamp = extract_timestamp(line, default_tzinfo)
                if entry.timestamp is not None:
                    if pattern:
                        if entry.grep(pattern, is_regex, case_sensitive):
                            entries.append(entry)
                    else:
                        entries.append(entry)
                    entry = LogEntry(logfile=filename, prefix=prefix)

    sorted_entries = sorted(entries, key=lambda entry: entry.timestamp)
    return LogEntries(sorted_entries)
