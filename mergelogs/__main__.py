from __future__ import print_function
import argparse

import dateutil.tz

from mergelogs import merge, to_datetime


OPENSTACK_VOLUME_ID_REGEX = (
    r'(?P<volume_id>[a-z0-9]{8}-'
    r'[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12})'
)

OPENSTACK_INSTANCE_ID_REGEX = (
    r'(?P<instance_id>[a-z0-9]{8}-'
    r'[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12})'
)

UUID_REGEX = r"[a-z0-9]{32}"

OPENSTACK_VOLUME_DETAILS_REGEX = (
    r".*"
    r"\[req-{volume_id_rx} "
    r"{uuid_rx} "
    r"{uuid_rx}\] "
    r".*"
    r"'migration_status': (?P<migration_status>[^,]+), "
    r"'availability_zone': (?P<availability_zone>[^,]+), "
    r"'terminated_at': (?P<terminated_at>[^,]+), "
    r"'updated_at': (?P<updated_at>[^\)]+\)), "
    r"'provider_geometry': .+"
    r"'deleted_at': (?P<deleted_at>[^,]+), "
    r"'id': u'(?P<vol_id>[^']+)'.+"
    r"'user_id': u'(?P<user_id>[^']+)'.+"
    r"'project_id': u'(?P<project_id>[^']+)', "
    r"'launched_at': (?P<launched_at>[^\)]+\)), "
    r"'scheduled_at': (?P<scheduled_at>[^\)]+\)), "
    r"'status': (?P<status>[^,]+), "
    r".+"
    r"'deleted': (?P<deleted>[^,]+), "
    r"'provider_location': u'(?P<ip>[0-9.]+):(?P<port>\d+),"
    r".+"
    r"'host': (?P<host>[^,]+), "
    r".+"
    r"'display_name': (?P<display_name>[^,]+), "
    r".+"
    r"'created_at': (?P<created_at>[^\)]+\)), "
    r".*"
).format(
    volume_id_rx=OPENSTACK_VOLUME_ID_REGEX,
    uuid_rx=UUID_REGEX,
    instance_id_rx=OPENSTACK_INSTANCE_ID_REGEX,
)

OPENSTACK_VOL_USER_PROJ_REGEX = (
    r".*"
    r"\[req-{volume_id_rx} "
    r"(?P<user_id>{uuid_rx}) "
    r"(?P<project_id>{uuid_rx})\] "
    r".*"
).format(
    volume_id_rx=OPENSTACK_VOLUME_ID_REGEX,
    uuid_rx=UUID_REGEX,
)

OPENSTACK_NOVA_DETAILS_REGEX = (
    r".*"
    r"\[req-{volume_id_rx} "
    r"(?P<user_id>{uuid_rx}) "
    r"(?P<project_id>{uuid_rx})\] "
    r"\[instance: {instance_id_rx}\]"
    r".*"
).format(
    volume_id_rx=OPENSTACK_VOLUME_ID_REGEX,
    uuid_rx=UUID_REGEX,
    instance_id_rx=OPENSTACK_INSTANCE_ID_REGEX,
)

IPV4_REGEX = (
    r'.*\b(?P<ip>((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}'
    r'(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))\b.*'
)

START = '2014-04-21 12:00'
END = '2014-04-21 23:59'


def by_value(dictionary, value):
    '''
    Return the first key of a dictionary having the given value
    '''
    return dictionary.keys()[dictionary.values().index(value)]


users = {
    'admin': 'e75374ede9834b41919d07e6a311401c',
    'animall': '253f4b4150a94040ac288bf8fd0dfea7',
    'frikandelfun': '2c159dbdd4ba4f1587ecd502afbe1fe1',
    'helipadsales': '847707c76b4940a99a221a3ce4f2dce4',
    'macos': '61b3f2a57e3041fa9cc956408bf2433b',
    'silkpath': '90dfd53b7d924654bff1f78d81591766',
    'ubishaft': 'ab9892d9cb7e40078be899c34c20b020',
}


def dict_unique(dictlist):
    '''
    Remove duplicates from a list of dictionaries
    '''
    unique_dictlist = []
    for dictionary in dictlist:
        if dictionary not in unique_dictlist:
            unique_dictlist.append(dictionary)
    return unique_dictlist


def instances(logs):
    return sorted(list(set([
        m.groupdict()['instance_id']
        for m in logs
        .in_files('.*nova.*')
        .in_range(START, END)
        .match(OPENSTACK_NOVA_DETAILS_REGEX)
    ])))


def report(logs, instance_id):
    ll = logs.grep(instance_id)
    report_lines = []

    # build
    lines = ll.grep('Attempting to build')
    for line in lines:
        m = line.match(OPENSTACK_VOL_USER_PROJ_REGEX)
        user = by_value(users, m.groupdict()['user_id'])
        report_lines.append((
            line.timestamp,
            'Creation started by {user}'.format(user=user)
        ))

    # start
    lines = ll.grep('Starting instance')
    for line in lines:
        m = line.match(OPENSTACK_VOL_USER_PROJ_REGEX)
        user = by_value(users, m.groupdict()['user_id'])
        report_lines.append((
            line.timestamp,
            'Instance started by {user}'.format(user=user)
        ))

    # size
    lines = ll.grep('Attempting Claim:')
    for line in lines:
        m = line.match('.*Attempting Claim: (?P<size>.+)')
        report_lines.append((
            line.timestamp,
            'Instance size: {size}'.format(size=m.groupdict()['size'])
        ))

    # run
    lines = ll.grep('Instance spawned successfully')
    for line in lines:
        report_lines.append((line.timestamp, 'Instance running'))

    # delete
    lines = ll.grep('DELETE', case_sensitive=True)
    for line in lines:
        m = line.match(OPENSTACK_VOL_USER_PROJ_REGEX)
        user = by_value(users, m.groupdict()['user_id'])
        report_lines.append((
            line.timestamp,
            'Requested deletion by {user}'.format(user=user)
        ))

    # volumes
    matches = ll.match(OPENSTACK_VOLUME_DETAILS_REGEX)
    for m in matches:
        vol = m.groupdict()
        report_lines.append((
            line.timestamp,
            'Volume {vol_id}, name={name}, status={status}'.format(
                vol_id=vol['vol_id'],
                name=vol['display_name'],
                status=vol['status'],
            )
        ))

    # destroy
    lines = ll.grep('Instance destroyed successfully')
    for line in lines:
        report_lines.append((
            line.timestamp,
            'Instance destroyed successfully'.format(ts=line.timestamp)
        ))

    report_lines = sorted(report_lines, key=lambda (ts, m): ts)
    for line in report_lines:
        print('{ts} {msg}'.format(ts=line[0], msg=line[1]))


def parse_args():
    parser = argparse.ArgumentParser(prog='mergelogs')
    parser.add_argument('files', nargs='+',
                        help='The input files or directories to merge. If an '
                             ' argument is a directory, all the files '
                             'contained at the first level are considered')
    parser.add_argument('--save', type=argparse.FileType('w'),
                        help='The merged and sorted log file')
    parser.add_argument('--start', default=None,
                        help='The start timestamp in '
                        'YYYY-mm-dd[ HH:MM:SS[.usec]] format')
    parser.add_argument('--end', default=None,
                        help='The end timestamp in '
                        'YYYY-mm-dd[ HH:MM:SS[.usec]] format')
    parser.add_argument('--stats', action='store_true', default=False,
                        help='Print some statistics on the logs')
    parser.add_argument('--verbose', action='store_true', default=False,
                        help='Print more verbose statistics')
    parser.add_argument('--shell', action='store_true', default=False,
                        help='Spawn an IPython shell after parsing')
    parser.add_argument('--grep', help='Filter by pattern, like grep. Case '
                        'insensitive by default (see --case-sensitive)')
    parser.add_argument('--case-sensitive', action='store_true',
                        default=False, help='Makes grep case sensitive')
    parser.add_argument('--regex', action='store_true',
                        help='Used with --grep, treats the pattern as a '
                        'regular expression')
    parser.add_argument('--default-timezone', default='UTC',
                        help='Set the default timestamp when a naive timestamp '
                        'is found. Can be either a name, like Europe/Rome, or '
                        'an offset, like +0100. Default is UTC')
    args = parser.parse_args()

    args.default_timezone = dateutil.tz.gettz(args.default_timezone)
    args.start = to_datetime(args.start)
    args.end = to_datetime(args.end)
    if args.regex and not args.grep:
        parser.error('Option --regex must be used in combination with --grep')
    return args


def main():
    args = parse_args()
    total_entries = merge(args.files, args.grep, args.regex,
                          args.default_timezone)
    entries_in_range = total_entries.in_range(args.start, args.end)
    if args.save:
        fd = args.save
        outfile = args.save.name
    else:
        outfile = 'merged.log'
        fd = open(outfile, 'w')
    for entry in entries_in_range:
        fd.write('{p} {m}'.format(p=entry.prefix, m=entry.message))

    if args.stats:
        print('Statistics')
        print('=' * 80)
        print(entries_in_range.stats(verbose=args.verbose))
        print('-' * 80)
        print('Excluded log entries: {n}'.format(
            n=len(total_entries) - len(entries_in_range)))

    print('Log entries saved to {f}. Time range: ({s}, {e})'
          .format(
            f=outfile,
            s=args.start,
            e=args.end,
          )
          )

    if args.shell:
        import IPython
        # this assignment is for visibility of a short-named variable in the
        # IPython shell
        logs = total_entries
        IPython.embed()


if __name__ == '__main__':
    main()
